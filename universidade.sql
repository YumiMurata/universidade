create table Aluno(
    id_aluno integer PRIMARY KEY ,
    matricula varchar(20),
    nome varchar(50),
    email varchar(50),
    telefone char(20),
    id_alunoTurma integer,
    FOREIGN KEY (id_alunoTurma) REFERENCES Turma(id_turma)

)
create table Monitoria(
    id_monitoria integer PRIMARY KEY,
    id_monitoriaAluno integer,
    id_monitoriaM integer
    FOREIGN KEY (id_monitoriaAluno) REFERENCES Aluno(id_aluno),
    FOREIGN KEY (id_monitoriaM) REFERENCES Materia(id_materia)

)
create table Professor(
    id_professor integer PRIMARY KEY,
    nome varchar(50),
    email varchar(50),
    telefone char(20),
    id_AlunoProf integer,
    id_profD integer,
    FOREIGN KEY (id_profD REFERENCES Departamento(id_departamento),
  
)

create table Departamento(
    id_departamento integer PRIMARY KEY,

)

create table Turma(
    id_turma integer PRIMARY KEY,
    quantAlunos integer,
    
)
create table Ministra(
    id_ministra integer PRIMARY KEY,
    id_minProf integer,
    id_minTurma integer,
    id_minMate integer,
    FOREIGN KEY (id_minProf REFERENCES Professor(id_professor),
    FOREIGN KEY (id_minTurma REFERENCES Turma(id_turma),
    FOREIGN KEY (id_minMate REFERENCES Materia(id_materia),

)

create table Curso (
    id_curso integer PRIMARY KEY,
    nome varchar(30),
    id_cursoDep integer,
    FOREIGN KEY (id_cursoDep REFERENCES Departamento(id_departamento),

)

create table Materia(
    id_materia integer PRIMARY KEY,
    nome varchar(30),

)

create table CursoMateria(
    id_CM integer PRIMARY KEY,
    id_materiaC integer,
    id_cursoM integer,
    FOREIGN KEY (id_materiaC) REFERENCES Materia(id_materia),
    FOREIGN KEY (id_cursoM) REFERENCES Curso(id_curso),
)
